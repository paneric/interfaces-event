<?php

declare(strict_types=1);

namespace Paneric\Interfaces\Event;

interface EventInterface
{
    public function getData(): mixed;

    public function setData(mixed $data): void;
}
